#Turl, A tinyurl.com clone
#Copyright (C) 2003  Mat Kovach (mkovach@alal.com)
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

ns_log Notice "Loading turl definitions"

proc turl_system_url {} {
    # The url of the system
    return "URL"
}

proc turl_system_name {} {
    # Name of the system
    return "SYSTEM TITLE"
}

proc turl_system_owner {} {
    # This is where all the email will go
    return "EMAILADDRESS"
}

proc turl_page_ext {} {
    # Valid page extentions to search for if none are give
    # Order is important as this is first come, frst to get servred
    return [list adp html htm]
}

proc turl_footer {} {
    # How do you want the footer to look like.  I like to included
    # a link to mail somebody
    return "<p>
<hr>
<a href=\"mailto:[turl_system_owner]\">
[turl_system_owner]
</a>
</body>
</html>"
}

proc turl_url {} {
    # what is the URL for this system 
    # This is used for the request processing.  If your system 
    # is http://domain/turl/ then use /turl/ here.
    return "/"
}

proc turl_url_offset {} {
    # number of additional urls
    # http://domain/turl/ would give an offset of 1
    return 0
}

proc turl_header { {title ""} } {
    # Top of the page stuff
    return "<html><head>
<title>$title</title>
</head>
<link rel=stylesheet type=\"text/css\" href=\"/turl-css.adp\">\n
<body>
<h2>$title</h2>
<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
  <tr>
    <td align=\"right\">
      <table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">
        <tr>
          <td bgcolor=\"#999999\" class=\"stripes\">&nbsp;</td>
        </tr>
        <tr>
          <td bgcolor=\"#0085c0\" class=\"stripes\">&nbsp;</td>
        </tr>
      </table></td>
    <td colspan=\"2\" width=\"80%\">
      <table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">
        <tr>
          <td bgcolor=\"#666666\" width=\"100%\" class=\"stripes\">&nbsp; &nbsp;</td>
        </tr>
        <tr>
          <td bgcolor=\"#0085c0\" width=\"100%\" class=\"stripes\">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<a href=\"/index.adp\">Home</a> : <a href=\"/about.adp\">About</a>
<hr>"
}

proc disable_days {} {
    # number of non working days to disable a redirection
    return 10
}

proc max_encode_length {} {
   # length of your turl id
   return 5
}

proc doc_root {} {
    # where is the root for the webpages
    return "/web/turl/www"
}
