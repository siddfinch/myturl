### Backup Script, pretty much the one Don Baccus include in his OpenACS docs
### I have just added a backup of all files, except the backup directory
proc backup {} {	
    set server "turl"
    set b "/usr/bin" 
    set d "/web/${server}"
    set bak "${d}/backup" 	  
    set tar "/bin/tar"
    set db [ns_db gethandle] 
    set sql "select date_part('day','today'::date) as day" 
    set selection [ns_db 1row $db $sql] 
    set_variables_after_query 
    set data "${server}_$day.dmp"
    set backup "${server}_$day.tar.gz" 
    ns_log Notice "Backup of [${server}_system_name] starting."
    ns_log Notice "pg_dump beginning..." 	
    if [catch {append msg [exec "$b/pg_dump" "${server}" ">$bak/$data"]} errmsg] { 
	ns_log Error "pg_dump failed: $errmsg" 
	ns_sendmail [${server}_system_owner] [${server}_system_owner] "[${server}_system_name] : pg_dump failed..." "$errmsg" 
	ns_db releasehandle $db 
	return 
    }	   
    append msg "\n"
    ns_log Notice "gzip of data beginning..." 	
    if [catch {append msg [exec "gzip" "-f" "$bak/$data"]} errmsg] { 
	ns_log Error "gzip of data failed: $errmsg" 
	ns_sendmail [${server}_system_owner] [${server}_system_owner] "[${server}_system_name] : gzip of data failed..." "$errmsg" 
	ns_db releasehandle $db 
	return 
    }	
    append msg "\n"

    ns_log Notice "Backup for filesystem started"
    if [catch {append msg [exec  "$tar" "-cPz" "--exclude=$bak/*" "--file" "$bak/$backup" "$d"]} errmsg] {
        ns_log Error "backup for files filed: $errmsg"
	ns_sendmail [${server}_system_owner] [${server}_system_owner] "[${server}_system_name] : backup for files failed..." "$errmsg" 
        ns_db releasehandle $db
        return
    }
    append msg "\n"
  
    ns_log Notice "Backup succeeded." 
    append msg "Backups succeeded"
    ns_sendmail [${server}_system_owner] [${server}_system_owner] "[${server}_system_name] : backup succeeded" "$msg" 
}	 

ns_share -init {set schedule_backup 0} schedule_backup
if {!$schedule_backup} { 
    ns_schedule_daily 0 0 backup 
    ns_log Notice "Backup has been scheduled." 
}
