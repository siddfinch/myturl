#Turl, A tinyurl.com clone
#Copyright (C) 2003  Mat Kovach (mkovach@alal.com)
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

ns_log Notice "Loading turl procs"

proc base62_func {} {
   # quick hack for our base 62 ids
   return [list  0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z]
}

proc get_base62 { n } {
   # convert a 0-61 number to our base62 id
   set base62 [base62_func]
   return [lsearch -exact $base62 $n]
}

proc print_base62 { n } {
   # print the index of the base62 id 0 -> 0, a -> 10, etc.
   set base62 [base62_func]
   return [lindex $base62 $n]
}

proc decode { num } {
   # given a id, decode to an integer
   # first make sure this is valid
   set length [max_encode_length]
   # make sure the id is correctly formated
   if { ![regexp {([0-9a-zA-Z]+)} $num] } {
        return -1
   }
   # Should really need this, but you never know
   if { [string length $num] != $length } {
	return -1
   }
   # This is the function that takes the id and creates a interger id
   # which we use in the database
   set max [expr $length - 1 ]
   set output 0
   foreach c [split $num {}] {
      set output [expr $output + [expr [get_base62 $c] * pow(62,$max)]]
      incr max -1
   }
   # return the id, as an integer
   return [expr int($output)]
}

proc convert { m } { 
   # given and interger, convert to an ID.
   if { $m < 62 } {
      return [print_base62 $m]
   } else {
      set line "[convert [expr $m / 62]][print_base62 [expr $m % 62]]" 
      return $line
   }
}

proc encode { num } {
   # convert a number to and ID and zero pad it
   # FIXME: we should throw and error if the id is longer than max_length
   set tmp [convert $num]
   set diff [expr [max_encode_length] - [string length $tmp]]
   for {set i 0} { $i < $diff } {incr i} {
      set tmp "0$tmp"
   }
   return $tmp
}

proc get_page { conn } {
 
   # this is the request processor
   
   # urls as a list
   set url_list      [ns_conn urlv $conn]

   # number of urls
   set url_list_size [ns_conn urlc $conn]

   # get our offset
   set offset        [turl_url_offset]

   # Index to start looking for requests
   set index [expr $url_list_size - $offset ]

   if { $index > 0 } {
      # we have /xxxxx which may or may not be a redirect
      set monitor_id [ decode  [ lindex $url_list $offset ] ] 
      # if the page does decode it is a redirect attempt
      if { $monitor_id != -1 } {
	 # Okay, lets see if it is a valid redirect
	 set db [ns_db gethandle]
	 set url [database_to_tcl_string_or_null $db "select url from turl where monitor_id=$monitor_id and enabled='t'"]
	 if {[empty_string_p $url]} {
	    # not a valid redirect, so we'll record an error
            ns_log Notice "Error, id $monitor_id not valid"
	 } else {
            # valid redirect, lets record some info and log it
	    set remote_addr [ns_conn peeraddr]
	    set referrer  [get_referrer]
	    set sql "insert into turl_click_log
(monitor_id,remote_addr,click_time,referrer)
values
($monitor_id,'[DoubleApos $remote_addr]',now(),'[DoubleApos $referrer]')"
	    if [catch {ns_db dml $db $sql} errmsg] {
	       ns_log Error "Could not log click for monitor $monitor_id, from $remote_addr/$referrer"
	    }

            # lets see if there is any extra information to include in the 
            # redirect

            # first log for extra url info
            set rest [lindex $url_list [expr $offset + 1] end ]
            if { ![empty_string_p $rest] } {
                append url "/$rest"
            }

            # now lets add any query information
    	    set query [ns_conn query $conn]
	    if {![empty_string_p $query]} {
	       append url "?$query"
	    }

            # do the redirection
	    ns_log Notice "redirecting to $url"
	    ns_returnredirect $url

            # close the connection
	    catch {ns_conn close $conn}

            # return 
            return -1
	 }
      } 
   }
   
   # if we get this far, we are not redirecting, therefore lets 
   # try to server the page

   # check, this might be the index
   if { [empty_string_p [lindex $url_list [turl_url_offset]]] } {
      # okay, set the url to /index
      set url "/index"
   } else {
      # get the url for the conenction info
      set url [ns_conn url $conn]
   }

   # add page_ in front of the url
   # with changes in the processor, this shouldn't be needed anymore
   # but this has not been tested ye.
   return "page_$url"
}

proc turl {conn ignored} {

  # lets get the page

  set page [get_page $conn]
  
  # if return was -1, they a redirection happened and we don't need to 
  # do anything.  This proc needs to be redone a bit.
    
  if { $page != -1 } {
      
      # pull the page info out and any extention 
      if { [regexp {^page_([^\.]+)(\.)?(.+)?} $page dummy file dot ext]} {
	 
          # setup the files we might be looking for 
	  set pagefile "[doc_root]${file}"
	  
	  # If we have an empty extention, add extentions to it 
	  if {[empty_string_p $ext]} {

              # first check if the page does not exists, if it does
              # server that file
	      if {![valid_pagefile_p $pagefile]} {

                  # okay, lets add extentions, look for a good file
                  foreach page_ext [turl_page_ext] {
                      if {[valid_pagefile_p "${pagefile}.${page_ext}"]} {

                          # we found a valid page, update pagefile and 
                          # stop processing
		          append pagefile ".$page_ext"
                          break
                      }
                  }
	      } else {
                  # Okay, it is not a valid page request, lets check for a directory.
                  if { [string compare [string range [ns_conn url $conn] end end] /] != 0 } {
		      # if we don't have a valid page but the url does not 
		      # END in a /, somebody might have been typing a dir
 		      # well try redirecting to that directory.
                      set url "[turl_system_url][ns_conn url $conn]/"
                      ns_returnredirect $url
                      return 0
                  }

		  # if ext is empty, but is a valid maybe it is a directory?
		  if { [file isdirectory $pagefile] } {

                      # should we just so a directory listing? Assume yes
		      set show_list_p 1

                      # look for directory files we might be able to use
 	              # ie: index.html, index.adp, etc.
		      foreach server_ext [get_directory_files] {
		          if { [valid_pagefile_p "${pagefile}/$server_ext"] } {

                              # Hey, we found one, update the pagefile and
                              # stop processing
			      append pagefile "/$server_ext"
			      set show_list_p 0
                              break
		          } 
                      }

                      # Do we still show the directory listing?
		      if { $show_list_p } { 

			  # not index.adp or index.html, show a directory
			  # listing and return
			  ns_return 200 text/html "[turl_header [turl_system_name]]\n[directory_listing $pagefile]\n<p>\n[turl_footer]\n"
			  catch {ns_close $conn}
			  return 0
		      }
		  } else {

                      # nothing to do, file not found
	              ns_returnnotfound
	              return
	          }
	      } 
	  } else {
              # Okay, lets actually just server the page and the extension
	      append pagefile ".${ext}"
          }

          # All that work, does the page even exist? 
	  if {![valid_pagefile_p $pagefile]} {
            
              # Nope, blah!
	      ns_returnnotfound
	      return
	  }
	 
          # Lets see if we can guess the file type, text/html, etc. 
	  set type [ns_guesstype $pagefile]
	 
          # is this a file we parse using the adp_parser? 
	  if {[string match "*[ns_config ns/server/[ns_info server]/adp map]" $pagefile]} {

              # yes, lets try to parse the page
	      set page [catch {ns_adp_parse -file $pagefile} error]
	      if {$page} {

                  # this is atually be the error
		  ns_return 200 text/html $error
	      } else {
                   
                  # Everying okay, lets return the page
		  ns_return 200 text/html $error
	      }  	
	  } else {
              # Nope, this is not parsed, just server the file
	      switch $type {
		  "*/*" {
		  }
		  default {
		      ns_returnfile 200 $type $pagefile
		  }
	      }
	  }
      }
  } else {

      # Nothing found, let the user no.
      ns_returnnotfound
  }
}

proc get_directory_files {} {
   # get the directory files from the server config
   return [split [ns_config "ns/server/[ns_info server]" directoryfile] ,]
}

proc valid_pagefile_p { pagefile } {
    # take a file and look for it on the system
    if {[catch {set fp [open $pagefile]}]} {
         return 0
      } else {
         return 1
      }
}

proc directory_listing { dir } {
    # taken directlry from OpenACS's reqeuest process
    # ad_proc -private rp_html_directory_listing 
    set list "
<table>
<tr align=left><th>File</th><th>Size</th><th>Date</th></tr>
<tr align=left><td colspan=3><a href=../>..</a></td></tr>
"

    # Loop through the files, adding a row to the table for each.
    foreach file [lsort [glob -nocomplain $dir/*]] {
	set tail [file tail $file]
	set link "<a href=$tail>$tail</a>"

	# Build the stat array containing information about the file.
	file stat $file stat
	set size [expr $stat(size) / 1000 + 1]K
	set mtime $stat(mtime)
	set time [clock format $mtime -format "%d-%h-%Y %H:%M"]

	# Write out the row.
	append list "<tr align=left><td>$link</td><td>$size</td><td>$time</td></tr>\n"
    }
    append list "</table>"
    return $list
}

proc valid_page_p { url } {
    # even a url, see if it works
    if {[util_link_responding_p $url]} {

        # if it is working, get the status
        if { [ catch {set status [util_get_http_status $url] } ] } {
            set status "500"
        }

    } else {
        
        # Borked
        set status "500"
    }

    # if there was a bad status, return 0
    if {$status == 500 || $status == 404} {
        return 0
    } else {

        # every seems okay
        return 1
    }
}

proc get_title { url } {
    # Attempt to get the title of the web page
    if { [catch {set page [ns_httpget $url]}] } {
        set title ""
    } else {
        if {![regexp {<title>(.*)?</title>} $page match title]} {
           set title ""
        }
    }
    return $title
}

proc print_turl_link { monitor_id title } {
    # prints a link for the turl
    return "<a href=\"[turl_system_url]/[encode $monitor_id]\"> $title - [turl_system_url]/[encode $monitor_id]</a><p><code>[turl_system_url]/[encode $monitor_id]</code><br>"
}

proc turl_link_to_clipboard { monitor_id } {
    # add some stuff to try and copy the turl link to the clip board
    return "<form>
<input type=hidden name=turl value=\"[turl_system_url]/[encode $monitor_id]\">
</form>
<script>
x = document.all.turl.createTextRange();
x.execCommand(\"Copy\");
</script>
</p>"
}

proc url_count { db url } {
    # how many times does url show up in the database (should be 0 or 1)
    return [database_to_tcl_string $db "select count(*) from turl where url='$url'"]
}

proc get_monitor_id_from_url { db url } {
    # give an url, get the monitor id
    return [database_to_tcl_string $db "select monitor_id from turl where url='$url'"]
}

proc get_monitor_id { db } {
    # get the next monitor id sequence
    return [database_to_tcl_string $db "select nextval('turl_monitor_id_seq')"]
}

proc add_url { url } {

    # trying to add a url to the database

    # this regexp is for Brent Welch's Pratical Programming in TCL/Tk
    if {![regexp {([^:]+)://([^:/]+)(:([0-9]+))?(/.*)?} $url match protocol host x serverport path]} {
        return "<b>You entered a bad url, make sure you have started the url with http://</b>"
    } else {
        
        # we got a url from that mess, let see if it works.
        if {![valid_page_p $url]} {
            # Nope, let the user know
            return "Error getting url<P>$url"
        } else {

            # we have a valid url, time to add it to the database
	    ns_log Notice "url to add: $url"

            # Lets get the title
            set title [get_title $url]

            # Open up a database handler
            set db [ns_db gethandle]

            # Lets prepare the url to be database friendly
	    set QQurl [DoubleApos $url]

            # Check the database to see if the url is already in the database
            if { [url_count $db $QQurl] > 0 } {

                # yep, somebody did it first, lets just update the database
                # showing that the link works
                set monitor_id [get_monitor_id_from_url $db $QQurl]
	        set sql "update turl set checked_date = now(),working_date = now(),enabled='t' where monitor_id = $monitor_id"
	        catch {ns_db dml $db $sql}
            } else {
                
                # Hey, it is a new url

                # get the next id
                set monitor_id [get_monitor_id $db]

                # this is just a hack to allow the admin to load the database
                # out of sequence and not cause errors
                while { [url_count $db $QQurl] > 0 } {
                    set monitor_id [get_monitor_id $db]
                }

                # lets stuff it into the database
                set sql "insert into turl (monitor_id,url,enabled,title,entered_date,checked_date,working_date) values ($monitor_id,'$QQurl','t','[DoubleApos $title]',now(),now(),now())"
                if { [catch {ns_db dml $db $sql} errmsg] } {

                    # Opps database error.
                    return "Database error"
                }
            }

            # return the link, the user won't know if the url was previously
            # entered or not.
            return "[print_turl_link $monitor_id $title]<p>[ turl_link_to_clipboard $monitor_id]"
        }
    }
}

proc list_of_urls { } {
    
    # Display a list of urls 
    set db [ns_db gethandle]
    set urls ""
    set selection [ns_db select $db "select monitor_id,url,title from turl order by monitor_id"]
    while { [ns_db getrow $db $selection] } {
        set_variables_after_query
	set turl "[turl_system_url]/[encode $monitor_id]"
        set link "<a href=\"$turl\">$turl</a>"
	if {![empty_string_p $title]} { 
            append urls "<li>$title - $link</li>\n"
        } else {
            append urls "<li>$link</li>\n"
        }
    }
    return $urls
}

proc click_count { {limit ""} } {
   
    # get the click count for links using limit to limit the amount of 
    # urls returned
    set db [ns_db gethandle]
    set output ""
    set sql "select t.monitor_id as monitor_id, t.title as title, count(*) as clicks from turl as t, turl_click_log as c 
where c.monitor_id = t.monitor_id 
group by t.monitor_id,t.title 
order by clicks desc"
    if {![empty_string_p $limit] } {
         append sql " limit $limit"
    }
    set selection [ns_db select $db $sql]
    while { [ns_db getrow $db $selection] } { 
        set_variables_after_query
        set turl "[turl_system_url]/[encode $monitor_id]"
        set link "<a href=\"$turl\">$turl</a>"
        append output "<tr><td>$link</td><td><a href=\"/clicks-for-one?turl_id=[encode $monitor_id]\">$title</a></td><td align=\"center\">$clicks</td></tr>\n"
    }
    return $output
}

proc clicks_for_one_url { monitor_id } {

    # display the clicks for, well, one url
    set db [ns_db gethandle]
    set output ""
    append output "<B>[get_url_info $db $monitor_id ]</B><hr align=\"center\" width=\"95%\">\n"
    append output "<table width=\"99%\" align=\"center\">\n"
    append output "<tr align=\"center\"><td>Remote Address</td><td>Referrer</td><td>Clicks</td></tr>\n"
    set sql "select remote_addr as addr ,referrer,count(*) as clicks 
from turl_click_log 
where monitor_id = $monitor_id
group by referrer,addr
order by clicks desc"
    set selection [ns_db select $db $sql]
    while { [ns_db getrow $db $selection] } {
        set_variables_after_query
        append output "<tr align=\"center\"><td>$addr</td><td>$referrer</td><td>$clicks</td></tr>\n"
    }
    append output "</table>\n"
    return $output
}
 
proc get_url_info { db monitor_id } {
    
     # get the information about a url
     set output "" 
     set selection [ns_db 0or1row $db "select title,url,entered_date from turl where monitor_id = $monitor_id"]
     set_variables_after_query
     if {![empty_string_p $title]} {
         append output "$title -"
     }
     append output "$url<br>entered:$entered_date"
     return $output
}

proc check_urls {} {

    # This checks the urls in the database (making sure they still work)
    set db [ns_db gethandle]
    set monitor_ids [database_to_tcl_list $db "select monitor_id from turl"]
    set n_urls [llength $monitor_ids]
    set start_time [ns_time]
    if { $n_urls == 0 } {
        ns_log Notice "Found no urls to monitor"
    } else {
	ns_log Notice "Starting to check $n_urls urls"
        foreach monitor_id $monitor_ids {
            set selection [ns_db 0or1row $db "select url,date_part('day',now() - working_date) as non_working_days from turl where monitor_id = $monitor_id "]
            if { $selection ==""} {
                #this should never happen
                continue
            }
            set_variables_after_query
            # update the database, that the url is being checked
	    set sql "update turl set checked_date = now() where monitor_id = $monitor_id"
	    if { [catch {ns_db dml $db $sql} errmsg] } {
                ns_log Error "Database error: $errmsg"
            }

            # lets see if it works
            if {[valid_page_p $url]} {

                # if it does, update the database
                set sql "update turl set working_date = now() where monitor_id = $monitor_id" 
                if { [catch {ns_db dml $db $sql} errmsg] } {
                    ns_log Error "Database error: $errmsg"
                }
            } else {

                # it stopped working
                if {$non_working_days > [disable_days]} {
                    set sql "update turl set enabled = 'f' where monitor_id = $monitor_id"
                    if { [catch {ns_db dml $db $sql} errmsg] } {
                        ns_log Error "Database error: $errmsg"
                    }
                } 
            }
        }
    }
    ns_log Notice "Took [expr [ns_time] - $start_time] seconds to check $n_urls urls"
}
 
ns_log Notice "Done loading turl procs"

# register our url handler
ns_register_proc GET  [turl_url] turl
ns_register_proc POST [turl_url] turl
ns_register_proc HEAD [turl_url] turl

ns_log Notice "Registred handled for [turl_url]"

# schedule our nightly checking
ns_share -init {set schedule_check_urls 0} schedule_check_urls
if {!$schedule_check_urls} {
    ns_schedule_daily 0 0 check_urls
    ns_log Notice "URL check has been scheduled."
}
