<%=[turl_header [turl_system_name]]%>

a tiny url system, provided as a service to the entire
Internet by <a href=http://www.alal.com>Alternative Linux Architecture Labs</a>. 
<hr>

Installation Instructions
<p>
<ol>
<li>You must have AOLserver, Postgres, and nspostgres modules installed.
<p>
<blocktext>
  <ul>
    <li><a href="http://www.aolserver.com">AOLserver</a>.
    <li><a href="http://myturl.com/0001g">AOLserver downloads</a>.
    <li><a href="http://myturl.com/0001h">AOLserver4 Beta 8</a>.
    <li><a href="http://myturl.com/0001i">Postgres Driver for AOLserver 4</a>
    <li><a href="http://myturl.com/0001j">AOLserver 3 downloads (includes Postgres driver)</a> from the <a href="http://www.openacs.org">OpenACS</a> group.
    <li><a href="http//www.postgresql.org">PostgreSQL</a>.
  </ul>
  <p>
  You can find AOLserver installation documentation from <a href="http://www.aolserver.com/docs/admin/install.html">AOLserver</a> or <a href="http://openacs.org/doc/openacs-4-6-3/aolserver.html">OpenACS</a>.
</blocktext>
<p>
<li>Grab the turl distribution <a href="http://myturl.com/software/dist/">http://myturl.com/software/dist/</a>
<p>
<li>Decieded where you want to install the software, I use /web, create the 
directory and extract the tar ball.
<blocktext>
<pre>
# mkdir /web (as root)
# chown nsadmin:web /web
# chmod 700 /web
# su - nsadmin
nsadmin@myturl: cd /tmp
nsadmin@myturl: wget http://myturl.com/software/dist/turl-current.tar.gz
nsadmin@myturl: cd /web
nsadmin@myturl: gzip -dc /tmp/turl-current.tar.gz| tar xvf -
</pre>
</blocktext>
<p>
<li>Edit the following files:
<blocktext>
<p>
<ul>
  <li>/web/turl/etc/turl.tcl
  <li>/web/turl/tcl/backup.tcl
  <li>/web/turl/tcl/tcl-defs.tcl
</ul>
</blocktext>
<p>
<li>Create the postgres database
<blocktext>
<pre>
# su - postgres
postgres@myturl: createuser nsadmin (answer yes to both questions)
postgres@myturl: exit
# su - nsadmin
nsadmin@myturl: createdb turl
nsadmin@myturl: psql -f /web/turl/www/software/sql/turl.sql turl
</pre>
</blockquote>
<p>
</ol>
Start the AOLserver for turl and everything should be running.
<p>

<%=[turl_footer]%>
