create sequence turl_monitor_id_seq;

create table turl (
	monitor_id	bigint
			constraint turl_monitor_id_pk
			primary key,
	url		text,
	enabled		boolean default 't' not null,
	title		text default 'A T-Url',
	entered_date	timestamptz,
	checked_date	timestamptz,
	working_date	timestamptz
);

create index turl_url_idx on turl ( url );
create index turl_title_idx on turl ( title );

create table turl_click_log (
	monitor_id	bigint not null references turl,
	remote_addr	varchar(50),
	click_time	timestamptz,
	referrer	text
);

create index turl_click_log_idx on turl_click_log ( monitor_id );
