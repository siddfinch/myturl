<%=[turl_header [turl_system_name]]%>

<a href="index.adp"><%=[turl_system_name]%></a> : Add url

<p>

<% 
set_form_variables
if { [info exists url] } {
    ns_puts " [add_url $url]
<p>
<B><I>
If you are running Internet Explorer 4.x or above, this URL /may/ 
be in your paste buffer (so you can paste it) but we are not promising 
anything.
</i></b>"
} else {
    ns_puts "<B> No URL given </B>"
}
%>

<p>

<%=[turl_footer]%>
