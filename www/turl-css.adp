BODY { background: #ffffff; font-family: Verdana, Arial, Helvetica; font-size: 12pt; }
H1   { font-family: Verdana, Arial, Helvetica; font-size: 18pt; }
H2   { font-family: Verdana, Arial, Helvetica; font-size: 16pt; }
H3   { font-family: Verdana, Arial, Helvetica; font-size: 14pt; }
PRE  { font-family: Verdana, Arial, Helvetica; font-size: 10pt; }
FORM { font-family: Verdana, Arial, Helvetica; font-size: 8pt; }
