<%=[turl_header "[turl_system_name]: Click count for one URL"]%>

<a href="index.adp"><%=[turl_system_name]%></a> :  Click count for one URL

<p>

<% 
set_form_variables
if {[empty_string_p $turl_id]} {
    ns_puts "No id given"
} else {
    set monitor_id [decode $turl_id]
    ns_puts "[clicks_for_one_url $monitor_id]"
}
%>

<p>

<%=[turl_footer]%>
