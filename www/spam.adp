<%=[turl_header [turl_system_name]]%>

a tiny url system, provided as a service to the entire
Internet by <a href="mailto:mek@mek.cc">mek</a>.
<hr>
<h2>Hey! You sent my SPAM "you c***sucking m****f***er, why don't you just die!"*</h2>
<i> * Actual quote from an email, after I corrected the spelliing</i>
<p>
A little info about SPAM and how it affects MYTURL.COM.  First
note the terms of service:
<blockquote>
<b>Terms of Use</b>
<p>
T url was created to make posting long URLs easier. Using it for spamming purposes is forbidden and any such use is simply stupid and you will be reported to all ISPs involved and to the proper governmental agencies.
<p>
This service and software is provided without warranty of any kind.
</blockquote>

But people will include MYTURL.COM links in SPAM. If this happens
I am sorry.  At the bottom of each web page is an email address, if 
you find an email that is SPAM and contains a MYTURL.COM link, I ask
a favor or few:
<ol>
<li>Forward a copy of the email to <code><a href="emailto:turl@myturl.com">turl@myturl.com</a></code></li>
<li>Report the spammer to the various real time blacklist sites, including <a href="http://spamhaus.org">Spamhaus</a>, <a href="http://spamcop.net">Spamcop</a>, and <a href="http://uribl.com">Uribl</a>.</li>
</ol>
Why? Well, if somebody does happen to ignore my Terms of Service, I would 
like the chance to investigate when the link was create and put additional
steps in place to attempt to make sure it doesn't happen again.
<p>
Next, MYTURL.COM uses many black listing services in an attempt to block
those links from being created. Just as with SPAM, it is very difficult
to keep up with new tactics that spammers will use. If people sending
spam are reported to the proper places, it makes it easier to get them 
blocked quicker.
<h2>How does MYTURL.COM attempt to block spammers?</h2>
MYTURL.COM does a number of things in an attempt to block SPAM.
<ul>
<li>We check the incoming address when somebody attempts to add a URL, 
if that incoming IP address is on a SPAM blacklist (also called Realtime
Blacklist or RBL), MYTURL.COM refuses the link and gives the spammer en
error message.</li>
<li>If the incoming IP address (or host) passes the above test, MYTURL.COM
then checks the URL and IP address of the given link against 5 realtime 
blacklist.  If the URL or IP address of the link is found to be on 
one of those lists, a MYTURL.COM link is not created and the user 
is given an error message.</li>
<li>If the link passes those two tests, the MYTURL.COM link is created.</li>
<li>Each time a MYTURL.COM link is clicked on, when the connection 
comes to the server, before the link is redirected it is check to see
if the link has turned up on a realtime blacklist.  If that link
is NOW on a blacklist, the link is *not* redirected and a message
is displayed.</li>
<li>Every two hours, all links that have been clicked in the two hour
period are checked to see if those links fail the blacklist testing. Failing
links are disabled</li>
</ul>

<p>

<%=[turl_footer]%>
