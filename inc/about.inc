<h3>About</h3
T url (or turl) is a <a href="http://tinyurl.com">TinyUrl</a> clone written
using <a href="http://aolserver.com">Aolserver</a> and 
<a href="http://www.postgresql.org">PostgresQL</a>
<p>
T url allows you to provide a short URL via email, USENET, or even on 
a web site.
<p>
It allows you to take a URL like:
<p>
<code>
http://www.mapquest.com/maps/map.adp?map.x=302&map.y=156&
mapdata=afbX8DbyAoesP8oMVXE9yH15Yqfri%252f0FnbmKKpfMbrjLn
0o8BLJ1%252bTQRc5bB8ZoqtlOeDdZwlJtHLgfMIVHUWlmPWw8uDAvn6M
%252bkyj2OhU7lZS%252fzgR6gc6Gc6UR0nFUKiKZ%252fUA1FA7i4Gox
VbNUmI3sVoXmLsVCjdi1tcAxjLEEXFdAvuJU%252bwjYfFeWO15n%252f
iFsgXNxKDxWULBFtyxoa65AuWb0a5SU%252ftWdT4P7e8CtC9acf37axZ
a%252fI2MWC7g54TPL6YB%252bwcKdZuh60N%252fb83BrfUSLSD%252f
fK1nJ16Ma8D%252fc%253d&click=center
</code>
<p>
and use:
<p>
<code>
<%=[turl_system_url]%>/0000f
</code>
<p>
T url checks the URL to make sure it is valid and attempts to look up
the title for each url.  Links are checked everyday to make sure they
are still active.  After 10 days, inactive links are disabled but still
checked.
<p>
<h3>Add TinyURL to your browsers toolbar</h3>
<p>Click and drag this link to your links toolbar.
    <blockquote>
        <a href="javascript:void(location.href='<%=[turl_system_url]%>/add-url?url='+location.href)"  onmouseover="window.status='';return true" onclick="return false">T Url</a></blockquote>
Once this is on your toolbar, you'll be able to make a T Url 
by clicking on the toolbar button, a T Url will be created for the page you 
are currently at.
</p>
<p>
T url checks the URL to make sure it is valid and attempts to look up 
the title for each url.  Links are checked everyday to make sure they 
are still active.  After 10 days, inactive links are disabled but still 
checked.
<p>
<h3>Redirection works!</h3>
<p>
You can setup your homepage with a turl:
<p>
<code>
http://people.myisp.com/~me
</code>
<p>
Creating a turl of, say:
<p>
<code>
<%=[turl_system_url]%>/01010
</code>
<p>
And you can post urls like:
<p>
<code>
<li><%=[turl_system_url]%>/01010?view-message.php=123</li>
<li><%=[turl_system_url]%>/01010/mypics</li>
</code>
<p>
<b>Terms of Use</b>
<p>
T url was created as a GPL'd version of TinyURL.
<p>
T url was created to make posting long URLs easier. Using it for spamming 
purposes is forbidden and any such use is simply stupid and you will be 
reported to all ISPs involved and to the proper governmental agencies. 
<p>
This service and software is provided without warranty of any kind.
