Turl, a tinyurl clone.
http://myturl.com/software/

Copyright (C) 2003  Mat Kovach (mkovach@alal.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

This software is provided without warranty of any kind.

Read the INSTALL for http://myturl.com/software/install.adp for installation 
documentation.

A mailing list has been setup for this software:

dev@myturl.com

Send an email to

dev-subscribe@myturl.com

to subscribe.
